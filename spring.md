# Spring
Can you do something unique, Spring? 

So to speak, a thing I haven’t seen before? 

I know what you bring: 

For I’ve seen your plants roar. 



Well, is that really a special case? 

Are you able to touch my heart and fill with love? 

Alas similar is summer’s face; 



Give me what makes you unique: 

And I shall return a grace never shown 

Then my interest you fully pique 

So I make your uniqueness well beknown. 



# about
Unfortunately the author didn't provide more information
