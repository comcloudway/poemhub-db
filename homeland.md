# Homeland by Jakob Meier & Franz Juling
I sit on a tree

I'm looking around

there flies a bee

a beautiful sound


I flap my wings

I feel the sun

a small bird sings

the mice they run


I look down at the world below

I take it in: the wonderful view

there on my right, the last patch of snow

to my left all the plants are new


I disconnect from the group

I tale my time

noone needs a troup

this life is mine


I sit on my tree

I'm looking around

it's where I feel free

my homeland I found




# about
This poem is written from a birds perspective, welcoming the spring.

It also focuses on the process of finding yourself and forming your life to your liking
